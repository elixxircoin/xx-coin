## This project contains the ERC1404 smart contract code for the initial distribution of xx coins

### xx coin

The **xx coin** is a digital currency that has been designed to be part of the decentralized ecosystem of the xx network.

The **xx network** will support decentralized messaging, payments, and dApps and was designed in response to growing public concern for **user privacy** as well as the emerging threat of **quantum computing**.

The **Elixxir communications layer** provides groundbreaking **privacy and security** by shredding user metadata while **Praxxis** provides a **denominated coin structure** that breaks payments into individual coins to provide privacy. The xx coins and the Praxxis breakthrough **blockchain consensus protocol** that enables them are based on distinctive hash-based cryptography, which is secure against attacks from current nation-state adversaries and future quantum computers.

In the genesis block, one billion xx coins will be minted. By MainNet, it is planned that the smart contracts will convert to official xx coins on the xx network, possibly via an intermediary smart contract.

### Timeline

**July 2018**: xx network private alpha released.

**August 2019**: xx network public alpha released.

**October 29th, 2019**: xx messenger released.

**December 3rd, 2019**: xx coin registration begins.

**January 7th, 2020**: xx coin sale opens.

**February 7th, 2020**: xx coin sale closes.

**February 9th, 2020**: xx coins ERC 1404 begin issue.

**April 2nd, 2020**: xx network beta released.

**1 Month prior to Main network**: Old ERC-1404 smart contract is shut down by disabling all transfer capabilities and replaced by smart contracts with redeemability to Main network.
Beta network will also be available for testing this redeemability function.

**Release of xx network MainNet (Q4 2020)**:
Smart contract coins released for trading/transfer. xx network sells remaining coins, up to 70%.

**6 Months after Main**:
Smart contract with redeemability shutdown no more than 6 months after Main network launch.
Foundation will claim all unclaimed smart contract coins, and attempt to contact owners directly.