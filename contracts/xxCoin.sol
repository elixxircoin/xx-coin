pragma solidity ^0.5.0;

import "@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol";
import "./managed-whitelist/ManagedWhitelistToken.sol";

contract xxCoin is ERC20Detailed, ManagedWhitelistToken {
  constructor (address initialAccount, uint256 initialBalance)
    ERC20Detailed("xxCoin", "xx", 0) 
    public
    {
      addToBothSendAndReceiveAllowed(initialAccount);
      _mint(initialAccount, initialBalance);
    }
}
